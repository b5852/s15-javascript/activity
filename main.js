// console.log("Hello World!");


// Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
// Use an alert for the total of 10 or greater and a console warning for the total of 9 or less
let numberOne = parseInt(prompt("Provide a number."));
let numberTwo = parseInt(prompt("Provide another number."));

let total = numberOne + numberTwo;
let grandTotal;

if(total < 10){
    // console.log(total);
    console.warn("The sum of the two numbers are: " + total);
}
else if(total >= 10 && total <= 20){
    grandTotal = numberTwo - numberOne;
    alert("The difference of the two numbers are: " + grandTotal);
}
else if(total >= 21 && total <= 29){
    grandTotal = numberOne * numberTwo;
    alert("The product of the two numbers are: " + grandTotal);
}
else{
    grandTotal = numberTwo / numberOne;
    alert("The division of the two numbers are: " + grandTotal);
}

// Prompt the user for their name and age and print out different alert messages based on the user input:
let name = prompt("What is your name?");
let age = parseInt(prompt("What is your age?"));

if(name == "" || age == ""){
    alert("Are you a time traveler?");
}
else{
    alert("Hello " + name + ". Your age is " + age);
}


// Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
function isLegalAge(){
    if(age > 17){
        return "You are of legal age.";
    }
    else{
        return "You are not allowed here.";
    }
}
alert(isLegalAge(age));


// Create a switch case statement that will check if the user's age input is within a certain set of expected input:
    switch(age){
        case 18:
            alert("You are now allowed to party");
            // break;
        case 21:
            alert("You are now part of adult society");
            break;
        case 65:
            alert("We thank you for your contribution to society");
            break;
        default:
            alert("Are you sure you're not an alien?");
            break;
    }



// Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.
    try{
        alert(isLeagalAge(age));
    }
    catch(error){
        console.warn(error.message);
    }
    finally{
        alert(isLegalAge(age));
    }


